import React from 'react'
import styled from "styled-components";

export const BasketFooter = ({total, clearOnClick}) => {
    return <StyledBasketFooter>
        <TotalPrice total={total}/>
        <StyledFooterButtons>
            <StyledClearButton onClick={clearOnClick}>Clear</StyledClearButton>
            <StyledCheckoutButton title="This button has not been implemented yet">Check Out ></StyledCheckoutButton>
        </StyledFooterButtons>
    </StyledBasketFooter>
};

export const TotalPrice = ({total}) => {
    return <StyledTotalPrice>
        $ {total.toFixed(2)}
    </StyledTotalPrice>
};

const StyledBasketFooter = styled.div`
    display: flex;
    background-color: #f8f9f7;
    width: 100%;
    height: 80px;
`;

const StyledTotalPrice = styled.div`
    margin-top: 10px;
    margin-left: 15px;
    margin-right: 67px;
    padding: 15px;
    font-weight: bold;
    font-size: 20px;
    width: 65px;
`;

const StyledFooterButtons = styled.div`
    display: flex;
    margin-top: 23px;
`;

export const StyledClearButton = styled.div`
    margin-right: 14px;
    margin-top: 9px;
    
    :hover{
        cursor: pointer;
    }
`;

const StyledCheckoutButton = styled.button`
    background-color: #6fbbe1;
    color: white;
    border-radius: 2px;
    border: none;
    height: 34px;
    width: 100px;
    
    :hover{
        cursor: pointer;
    }
`;