import React from 'react'
import styled from 'styled-components'

export const BasketRow = ({name, price, quantity, onQuantityChange, clearItemOnClick}) => {
    return <StyledRow>
        <td><StyledName>{name}</StyledName></td>
        <td><StyledQuantityInput value={quantity} onChange={onQuantityChange}/></td>
        <td><StyledPrice>${price.toFixed(2)}</StyledPrice></td>
        <td><StyledClear onClick={clearItemOnClick}>X</StyledClear></td>
    </StyledRow>
};

const StyledName = styled.div`
    padding: 20px 0;
    padding-right: 30px
`;

const StyledQuantityInput = styled.input`
    width: 14px;
    height: 14px;
    margin-right: 15px;
    text-align: center;
    
    :hover{
        cursor: pointer;
    }
`;

const StyledRow = styled.tr`
    border: dashed;
    border-width: 1px 0;
    border-color: #ceccc9;
;
    
    :first-child {
        border-top: none;
    }
    
    :last-child {
        border-bottom: none;
    }
`;

export const StyledPrice = styled.div`
    color: #e59569;
    width: 60px;
    text-align: right;
`;

export const StyledClear = styled.div`
    margin-left: 5px;
    color: #ceccc9;
    
    :hover{
        cursor: pointer;
    }
`;