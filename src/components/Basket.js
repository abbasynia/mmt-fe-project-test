import React from 'react'
import styled from 'styled-components'
import {BasketRow} from "./BasketRow";
import {BasketFooter} from "./BasketFooter";

const initialBasketState = [
    {
        name: 'Mountain Dew',
        itemPrice: 1.80,
        quantity: 0,
    },
    {
        name: 'Desperados',
        itemPrice: 2.58,
        quantity: 0,
    },
    {
        name: 'Jack Daniels',
        itemPrice: 3.35,
        quantity: 0,
    },
];

export class Basket extends React.Component {
    constructor() {
        super();
        this.state = {
            basketData: initialBasketState.slice(0)
        };
        this._updateItemQuantity = this.updateItemQuantity.bind(this);
    }

    render() {
        return <StyledBasket>
            <StyledItemsTable>
                <tbody>
                    {this.state.basketData.map(({name, itemPrice, quantity}, index) =>
                        <BasketRow
                            key={index}
                            name={name}
                            price={quantity * itemPrice}
                            quantity={quantity}
                            onQuantityChange={(event) => {
                                const value = event.target.value;
                                this._updateItemQuantity(index, value);
                            }}
                            clearItemOnClick={() => this._updateItemQuantity(index, 0)}
                        />)
                    }
                </tbody>
            </StyledItemsTable>
            <BasketFooter
                total={this.state.basketData.reduce((acc, item) =>  {
                    return item.itemPrice * item.quantity + acc;}, 0)}
                clearOnClick={() => {
                    this.setState({basketData: initialBasketState.slice(0)})
                }}
                />
        </StyledBasket>
    }

    updateItemQuantity(itemIndex, newValue) {
        const basketToUpdate = this.state.basketData;
        basketToUpdate[itemIndex] = {...basketToUpdate[itemIndex], quantity: newValue};
        this.setState({
            basketData: basketToUpdate
        }, )
    }
}

const StyledItemsTable = styled.table`
    border-collapse: collapse;
    width: 85%;
`;

const StyledBasket = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    background-color: white;
    width: 350px;
    margin: 0 auto;
    border-radius: 3px;
`;
