import React from 'react';
import {default as Enzyme, mount} from 'enzyme';
import {expect} from 'chai';
import {Basket} from "../components/Basket";
import Adapter from "enzyme-adapter-react-16";
import {BasketRow, StyledClear, StyledPrice} from "../components/BasketRow";
import {BasketFooter, StyledClearButton, TotalPrice} from "../components/BasketFooter";

Enzyme.configure({ adapter: new Adapter() });
let basket;

beforeEach(() => {
    basket = mount(<Basket/>);
});

it('shows the initial total value of the basket', () => {
    expect(basket.find(TotalPrice).text()).to.equal("$ 0.00");
});

it('shows the total price paid for an item when the quantity is changed', () => {
    // Price of first item is $1.80
    basket.find(BasketRow).at(0).find('input').simulate('change', { target: { value: 1 } });
    expect(basket.find(BasketRow).at(0).find(StyledPrice).text()).to.equal("$1.80");

    basket.find(BasketRow).at(0).find('input').simulate('change', { target: { value: 2 } });
    expect(basket.find(BasketRow).at(0).find(StyledPrice).text()).to.equal("$3.60");
});

it('shows the total price paid overall for all items and quantities', () => {
    // Price of first item is $1.80
    basket.find(BasketRow).at(0).find('input').simulate('change', { target: { value: 3 } });
    // Price of second item is $2.58
    basket.find(BasketRow).at(1).find('input').simulate('change', { target: { value: 2 } });

    const expectedTotal = 1.8 * 3 + 2.58 * 2;
    expect(basket.find(TotalPrice).text()).to.equal(`$ ${expectedTotal}`);
});

it('clears the total back to 0 after the clear button is pressed', () => {
    // Price of first item is $1.80
    basket.find(BasketRow).at(0).find('input').simulate('change', { target: { value: 1 } });

    expect(basket.find(TotalPrice).text()).to.equal(`$ 1.80`);

    basket.find(StyledClearButton).simulate('click');

    expect(basket.find(TotalPrice).text()).to.equal(`$ 0.00`);
});

it('clears quantity and price of item back to 0 when X is pressed', () => {
    // Price of first item is $1.80
    basket.find(BasketRow).at(0).find('input').simulate('change', { target: { value: 1 } });

    expect(basket.find(BasketRow).at(0).find(StyledPrice).text()).to.equal("$1.80");

    basket.find(StyledClear).at(0).simulate('click');

    expect(basket.find(BasketRow).at(0).find(StyledPrice).text()).to.equal("$0.00");
    expect(basket.find(TotalPrice).text()).to.equal(`$ 0.00`);
});
